<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">   <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Sign-up-in</title>
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="https://cdn.rawgit.com/PascaleBeier/bootstrap-validate/v2.2.0/dist/bootstrap-validate.js" ></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
</head>
<body>   
  <div class="alert col-md-12 d-flex justify-content-center">
      <?php if(!empty($pageData['message'])) :?>
          <p class="text-success"><?php echo $pageData['message']; ?> Now you can login</p>          
      <?php endif; ?>
      <?php if(!empty($pageData['error'])) :?>
          <p class="text-danger"><?php echo $pageData['error']; ?></p>
      <?php endif; ?>
  </div>
  <div class="alert col-md-12 d-flex justify-content-center">
      <?php if(!empty($pageData['messageLogin'])) :?>
          <p class="text-success"><?php echo $pageData['messageLogin']; ?></p>
          <p class="text-secondary"><a href="/login/logout">Logout</a></p>
      <?php endif; ?>
      <?php if(!empty($pageData['errorLogin'])) :?>
          <p class="text-danger"><?php echo $pageData['errorLogin']; ?></p>
      <?php endif; ?>
  </div>
  <div class="container">
    <div class="sign-box">
      <div class="col-md-12 m-header d-flex p-4 bg-info text-white">        
          <div id="div-icon" class="pr-4">
            <div class="div-icon d-flex justify-content-center align-items-center">
                <i class="fa fa-2x fa-edit"></i>
            </div>               
          </div>
          <div id="m-header" class="p-2 d-flex justify-content-start align-items-center">
              <h3>Login To Your Account / Register New</h3>
              <div style="display: none; font-size: 18px">Login / Register</div>
          </div>        
      </div>
      <div class="m-body bg-light d-flex p-2">
        <div class="login col-md-6 pr-2">
          <form class="needs-validation" novalidate  action="login" method="post">
            <div class="m-body-login form-group d-flex justify-content-center pb-4">  
              <div class="m-body-login-top">
              </div>
              <div class="m-login-abs d-flex justify-content-center">
                <span class="span-login-abs">/</span>
              </div>              
            </div>
            <div class="form-group pb-2">            
              <input type="text" name="name" id="validationInput" class="form-control form-control-lg" placeholder="User Name" required>              
            </div>
            <div class="form-group pb-2">            
              <input type="password" minlength="6" name="password" class="form-control form-control-lg" placeholder="Password" required>
            </div>    
            <div id="check-btn" class="d-flex justify-content-between">
              <div data-placement='bottom' data-html="true"
                data-original-title="You no longer need to enter<br> a password every time you use the site" 
                data-toggle='tooltip' class="form-check d-flex align-items-center justify-content-start">
                <label class="form-check-label text-muted">
                  <input type="checkbox" class="form-check-input" value=""><span class="span-checkbox">Remember My password</span>
                </label>
              </div>
              <button type="submit" class="d-flex justify-content-center btn btn-mod grad-1">
                <span>Login</span>
              </button>
            </div>                  
          </form>
        </div>
        <hr id="hr" style="display:none">
        <div class="register col-md-6">
          <div class="difLine" style="display: block"></div>            
              <form class="needs-validation f-reg pl-4" novalidate action="" method="post">
                  <div class="f-reg-title form-group text-muted">            
                    <h4 class="">Register</h4>
                  </div>
                <div class="form-group text-muted">
                  <label>Email</label>
                  <input type="email" name="email" class="form-control" required>
                </div>
                <div class="form-group text-muted">
                  <label>User Name</label>
                  <input type="text" name="name" class="form-control" required>
                </div>
                <div class="form-group text-muted">
                  <label>Password</label>
                  <input type="password" minlength="6" name="password" class="form-control" required>
                </div>    
                <div class="btn-2 pt-2 d-flex justify-content-end">            
                  <button type="submit" class="btn btn-mod grad-2"> Register </button>
                </div>
              </form>            
          </div>          
        </div>
      </div>
    </div>   
  </div> 
  <!-- Bootstrap validate -->
  <script>    
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
  </script>
  <!-- Tooltip for remember password -->
  <script>     
      $(function () {
           $("[data-toggle='tooltip']").tooltip();
       });   
   </script>
</body>
</html>

