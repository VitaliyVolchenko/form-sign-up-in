<?php

class LoginController extends Controller
{
    private $pageTpl = '/views/main.tpl.php';

    public function __construct()
    {
        $this->model = new LoginModel();
        $this->view = new View();
    }

    public function index()
    {      
        //$this->pageData['title'] = "Login / Register";
        if(!empty($_POST)){           
            if(!$this->login()){
               $this->pageData['errorLogin'] = "Login failed. Username not exists please try again";
            } else {
               $this->pageData['messageLogin'] = "LOGIN SUCCESSFUL!!! ";
            }    
        }        
        $this->view->render($this->pageTpl, $this->pageData);        
    }
    
    public function login()
    {      
      if(!$this->model->login()){
         return false;
      } else {
         return true;
      }  
    }

    public function logout()
    {
        session_destroy();
        header("Location: /");
    }
  

}