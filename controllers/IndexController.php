<?php

class IndexController extends Controller
{
    private $pageTpl = '/views/main.tpl.php';

    public function __construct()
    {
        $this->model = new IndexModel();
        $this->view = new View();
    }

    public function index()
    {       
        //$this->pageData['title'] = "Login / Register";
        if(!empty($_POST)){           
            if(!$this->reg()){
               $this->pageData['error'] = "Register failed. Username or Email already exists please try again";
            } else {
               $this->pageData['message'] = "REGISTER SUCCESSFUL!!!";
            }            
        }        
        $this->view->render($this->pageTpl, $this->pageData);        
    }

    public function reg()
    {
        if(!$this->model->regUser()){
            return false;
        } else {
            return true;
        }        
    } 
    
    public function login()
    {      
      var_dump(111111);
    }  

}